import json

from django import forms
import requests
from account.models import OAuthService
from permissions.models import AzureGroup


class AzureGroupForm(forms.ModelForm):
    class Meta:
        model = AzureGroup
        fields = ("group", "azure_id", "azure_name")

    azure_id = forms.ChoiceField(
        choices=(("", "Please enable Office 365 for this app"),)
    )

    azure_name = forms.CharField(widget=forms.HiddenInput(), required=False)

    @staticmethod
    def get_groups():
        """
        Gets the 0ffice365 groups using the OAuthService token
        and return an array of Azure group tuples (id, name)
        """

        service = OAuthService.objects.filter(name="microsoft").first()
        if service is None:
            return (("", "Please enable Office 365 for this app"),)
        data = requests.get(
            service.provider.graph("/groups?$orderby=displayName"),
            headers={
                "Authorization": "Bearer {}".format(
                    service.provider.retrieve_app_token()["access_token"]
                )
            },
        ).json()

        if data.get("error"):
            return (
                (
                    "",
                    "Error while fetching groups : {}".format(
                        data.get("error").get("message")
                    ),
                ),
            )

        groups = data.get("value", [])
        return [(group["id"], group["displayName"]) for group in groups]

    def __init__(self, *args, **kwargs):
        super(AzureGroupForm, self).__init__(*args, **kwargs)

        self.group_choices = AzureGroupForm.get_groups()
        self.fields["azure_id"].choices = self.group_choices
        self.initial["azure_id"] = self.instance.azure_id

    def clean(self):
        super().clean()

        # Iterate over the groups to find the name associated with the chosen id
        for group in self.group_choices:
            if self.cleaned_data["azure_id"] == group[0]:
                self.cleaned_data["azure_name"] = group[1]
