import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.template import Template, Context
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, TemplateView

from app.models import ComptaDocument


class DocumentFormattedView(DetailView):
    model = ComptaDocument
    template_name = "app/document_formatted.html"
    context_object_name = "document"
    decorators = [login_required]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.object.prefix.type.due_text:
            return context

        template = Template(self.object.prefix.type.due_text.content)
        context["due_text"] = template.render(Context(dict(document=self.object)))

        template = Template(self.object.prefix.type.reminder_text)
        reminder_text = (
            template.render(Context(dict(document=self.object)))
            .replace(" ", "%20")
            .replace("\n", "%0D%0A")
        )
        context["reminder_text"] = reminder_text

        context[
            "reminder_subject"
        ] = f"Relance {self.object.prefix.type.name} {self.object}".replace(" ", "%20")

        context["today"] = datetime.date.today()

        return context

    @method_decorator(decorators)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class PublicHomeView(TemplateView):
    template_name = "app/public_home.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse("admin:index"))
        else:
            return super().get(request, *args, **kwargs)
