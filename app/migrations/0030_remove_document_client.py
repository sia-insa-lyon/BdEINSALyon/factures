# Generated by Django 2.0.1 on 2018-01-30 11:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0029_auto_20180130_1215'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='client',
        ),
    ]
