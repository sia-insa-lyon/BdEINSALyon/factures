# Generated by Django 2.0.1 on 2018-01-30 14:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0032_client_activated'),
    ]

    operations = [
        migrations.AddField(
            model_name='type',
            name='payable',
            field=models.BooleanField(default=False, verbose_name='payable'),
        ),
    ]
