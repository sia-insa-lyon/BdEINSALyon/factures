from django.contrib import admin

from app import forms
from app.models import (
    ComptaType,
    ComptaDocument,
    VatRate,
    ComptaLine,
    Emitter,
    ComptaPrefix,
    DueText,
    LegalMentions,
    Client,
    PaymentMethod,
)


@admin.register(DueText)
class DueTextAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "content",
    ]
    list_display_links = ["id"]


@admin.register(LegalMentions)
class LegalMentionsAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "content",
    ]
    list_display_links = ["id"]


@admin.register(ComptaType)
class ComptaTypeAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "display_bank_details",
        "due_text",
    ]
    filter_horizontal = [
        "legal_mentions",
    ]
    list_display_links = ["name"]
    search_fields = [
        "name",
    ]
    fieldsets = [
        (None, {"fields": ("name",)}),
        (
            "Options",
            {
                "fields": (
                    "legal_mentions",
                    "due_text",
                    "display_bank_details",
                    "reminder_text",
                )
            },
        ),
        ("Avancé", {"classes": ("collapse",), "fields": ("payable",)}),
    ]


@admin.register(PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    pass


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "email",
        "zip_code",
        "city",
        "country",
        "created_at",
        "updated_at",
    ]
    list_display_links = [
        "name",
    ]
    search_fields = [
        "name",
        "email",
        "address",
        "city",
        "zip_code",
        "country",
    ]
    date_hierarchy = "updated_at"
    fieldsets = [
        (
            "Contact",
            {"fields": ("name", "email", "address", ("zip_code", "city"), "country")},
        ),
        ("Avancé", {"classes": ("collapse",), "fields": ("activated",)}),
    ]


class ComptaLineInline(admin.TabularInline):
    model = ComptaLine
    form = forms.ComptaLineAdminForm


@admin.register(ComptaDocument)
class ComptaDocumentAdmin(admin.ModelAdmin):
    form = forms.ComptaDocumentAdminForm
    list_display = [
        "__str__",
        "client_name",
        "emitter",
        "displayed_date",
        "due_date",
        "total_excluding_vat",
        "total_vat",
        "total_including_vat",
        "payment_method",
    ]
    fieldsets = [
        (
            "Informations générales",
            {
                "fields": (
                    ("prefix", "sequence_number"),
                    "emitter",
                    "displayed_date",
                    "due_date",
                    "payment_method",
                    "created_at",
                    "updated_at",
                )
            },
        ),
        (
            "Client",
            {
                "fields": (
                    "client",
                    "client_name",
                    "client_email",
                    "client_address",
                    ("client_zip_code", "client_city"),
                    "client_country",
                    "save_client",
                )
            },
        ),
        ("Autres informations", {"fields": ("free_text",)}),
    ]
    readonly_fields = ["updated_at", "created_at"]
    list_display_links = [
        "__str__",
    ]
    list_filter = [
        "prefix",
        "payment_method",
    ]
    search_fields = [
        "sequence_number",
    ]
    date_hierarchy = "due_date"
    inlines = [ComptaLineInline]
    actions = ["duplicate_document"]
    ordering = ["prefix__value", "-sequence_number"]

    def duplicate_document(self, request, queryset):
        for doc in queryset:
            new_document = ComptaDocument.objects.create(
                prefix=doc.prefix,
                client_name=doc.client_name,
                client_address=doc.client_address,
                client_zip_code=doc.client_zip_code,
                client_city=doc.client_city,
                client_country=doc.client_country,
                emitter=doc.emitter,
                due_date=doc.due_date,
                displayed_date=doc.displayed_date,
            )
            for l in doc.compta_lines.all():
                ComptaLine.objects.create(
                    label=l.label,
                    unit_price=l.unit_price,
                    including_vat_fixed=l.including_vat_fixed,
                    quantity=l.quantity,
                    vat_rate=l.vat_rate,
                    document=new_document,
                )

    duplicate_document.short_description = "Dupliquer les documents sélectionnés"


@admin.register(VatRate)
class VatRateAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "rate",
        "activated",
        "created_at",
        "updated_at",
    ]
    list_display_links = [
        "name",
    ]
    list_filter = ["activated"]
    search_fields = [
        "name",
    ]
    date_hierarchy = "updated_at"
    fieldsets = [
        (None, {"fields": ("name", "rate")}),
        ("Avancé", {"classes": ("collapse",), "fields": ("activated",)}),
    ]


@admin.register(ComptaLine)
class ComptaLineAdmin(admin.ModelAdmin):
    list_display = [
        "label",
        "unit_price",
        "including_vat_fixed",
        "quantity",
        "vat_rate",
        "document",
        "created_at",
        "updated_at",
    ]
    list_display_links = [
        "label",
    ]
    list_filter = ["vat_rate", "including_vat_fixed"]
    search_fields = [
        "label",
    ]
    date_hierarchy = "document__due_date"


@admin.register(Emitter)
class EmitterAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "zip_code",
        "city",
        "country",
    ]
    list_display_links = [
        "name",
    ]
    search_fields = [
        "name",
        "zip_code",
        "city",
        "country",
        "phone",
        "fax",
        "siret",
        "vat_number",
        "bank",
        "iban",
        "bic",
    ]
    fieldsets = [
        ("Adresse", {"fields": ("name", "address", ("zip_code", "city"), "country")}),
        ("Contact", {"fields": (("phone", "fax"),)}),
        ("Entreprise", {"fields": ("logo_url", "siret", "vat_number")}),
        ("Coordonnées bancaires", {"fields": ("bank", "iban", "bic")}),
    ]
    date_hierarchy = "updated_at"


@admin.register(ComptaPrefix)
class ComptaPrefixAdmin(admin.ModelAdmin):
    list_display = [
        "value",
        "type",
        "activated",
        "current_sequence_number",
        "created_at",
        "updated_at",
    ]
    list_display_links = [
        "value",
    ]
    list_filter = ["type", "activated"]
    search_fields = [
        "value",
        "current_sequence_number",
    ]
    date_hierarchy = "updated_at"
    fieldsets = [
        (None, {"fields": ("value", "type", "current_sequence_number")}),
        ("Avancé", {"classes": ("collapse",), "fields": ("activated",)}),
    ]
