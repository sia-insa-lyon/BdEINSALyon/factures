from django import forms
from django.db.models import Q

from app.models import Client, ComptaDocument, ComptaPrefix, ComptaLine, VatRate


class ComptaDocumentAdminForm(forms.ModelForm):
    class Meta:
        model = ComptaDocument
        fields = "__all__"

    client = forms.ModelChoiceField(
        Client.objects.filter(activated=True).order_by("name"),
        required=False,
        help_text="Remplace les champs ci-dessous par les données du client sélectionné.",
    )
    save_client = forms.BooleanField(
        required=False, label="Enregistrer le client", initial=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields["prefix"].queryset = (
                ComptaPrefix.objects.filter(
                    Q(activated=True) | Q(documents=self.instance)
                )
                .distinct()
                .order_by("value")
            )
            self.fields["prefix"].initial = self.instance.prefix
        else:
            self.fields["prefix"].queryset = (
                ComptaPrefix.objects.filter(activated=True).distinct().order_by("value")
            )
        self.fields["sequence_number"].disabled = True

    def clean(self):
        client = self.cleaned_data.get("client", None)
        if client:
            self.cleaned_data["client_name"] = client.name
            self.cleaned_data["client_email"] = client.email
            self.cleaned_data["client_address"] = client.address
            self.cleaned_data["client_zip_code"] = client.zip_code
            self.cleaned_data["client_city"] = client.city
            self.cleaned_data["client_country"] = client.country
        else:
            error_in_client = False
            required_data = {
                "client_name": self.cleaned_data.get("client_name"),
                "client_zip_code": self.cleaned_data.get("client_zip_code"),
                "client_city": self.cleaned_data.get("client_city"),
            }
            for k, v in required_data.items():
                if v is None or v == "":
                    error_in_client = True
                    self.add_error(
                        k,
                        forms.ValidationError(
                            "Ce champ est requis si vous ne choisissez pas de client.",
                            code="required",
                        ),
                    )

            if (
                not self.errors
                and not error_in_client
                and self.cleaned_data["save_client"]
            ):
                search = Client.objects.filter(
                    name__iexact=self.cleaned_data["client_name"]
                )
                if search.count() > 0:
                    self.add_error(
                        "client_name",
                        forms.ValidationError(
                            "Un client avec ce nom existe déjà.", code="conflict"
                        ),
                    )
                else:
                    Client.objects.create(
                        name=self.cleaned_data["client_name"],
                        email=self.cleaned_data["client_email"],
                        address=self.cleaned_data["client_address"],
                        zip_code=self.cleaned_data["client_zip_code"],
                        city=self.cleaned_data["client_city"],
                        country=self.cleaned_data["client_country"],
                    )

        if "prefix" not in self.cleaned_data:
            return self.cleaned_data

        prefix = self.cleaned_data["prefix"]
        if self.instance.pk and self.instance.prefix != prefix:
            if (
                self.instance
                != ComptaDocument.objects.filter(prefix=self.instance.prefix).last()
            ):
                self.add_error(
                    "prefix",
                    forms.ValidationError(
                        "Des documents avec un numéro ultérieur existent "
                        "pour le préfixe actuel. Modifier le préfixe "
                        "introduirait une incohérence dans la séquence de "
                        "documents.",
                        code="not-last",
                    ),
                )
            elif not self.errors:
                self.instance.prefix.current_sequence_number -= 1
                self.instance.prefix.save()
                prefix.current_sequence_number += 1
                prefix.save()
                self.cleaned_data["sequence_number"] = prefix.current_sequence_number

        return self.cleaned_data


class ComptaLineAdminForm(forms.ModelForm):
    class Meta:
        model = ComptaLine
        fields = "__all__"

    vat_rate = forms.ModelChoiceField(VatRate.objects.none())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields["vat_rate"].queryset = (
                VatRate.objects.filter(
                    Q(activated=True) | Q(compta_lines=self.instance)
                )
                .distinct()
                .order_by("name")
            )
            self.fields["vat_rate"].initial = self.instance.vat_rate
        else:
            self.fields["vat_rate"].queryset = (
                VatRate.objects.filter(activated=True).distinct().order_by("name")
            )
