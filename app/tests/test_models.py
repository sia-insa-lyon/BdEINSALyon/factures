import datetime
from decimal import Decimal

from django.test import TestCase

from app.models import (
    ComptaDocument,
    ComptaType,
    ComptaLine,
    VatRate,
    ComptaPrefix,
    Emitter,
)


class CostsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        type = ComptaType.objects.create(name="Facture")
        prefix = ComptaPrefix.objects.create(value="Fe1718/", type=type)
        emitter = Emitter.objects.create(
            name="test emitter",
            address="hello",
            zip_code="12345",
            city="hello",
            country="hello",
            bank="hello",
            iban="FR7600000000000000000000000",
            bic="hello",
        )
        cls.document = ComptaDocument.objects.create(
            client_name="test client",
            client_address="hello",
            client_zip_code="12345",
            client_city="hello",
            client_country="hello",
            prefix=prefix,
            emitter=emitter,
            due_date=datetime.date.today(),
            displayed_date=datetime.date.today(),
        )
        cls.vat = VatRate.objects.create(name="TVA 20%", rate=Decimal("1.2"))

    def setUp(self):
        self.lines = [
            {
                "line": ComptaLine.objects.create(
                    document=self.document,
                    vat_rate=self.vat,
                    label="line",
                    unit_price=Decimal("20.2"),
                    including_vat_fixed=True,
                    quantity=Decimal("1"),
                ),
                "expected_inc": Decimal("20.2"),
                "expected_exc": Decimal("16.83"),
                "expected_vat": Decimal("3.37"),
            },
            {
                "line": ComptaLine.objects.create(
                    document=self.document,
                    vat_rate=self.vat,
                    label="line",
                    unit_price=Decimal("20.2"),
                    including_vat_fixed=False,
                    quantity=Decimal("1"),
                ),
                "expected_inc": Decimal("24.24"),
                "expected_exc": Decimal("20.2"),
                "expected_vat": Decimal("4.04"),
            },
            {
                "line": ComptaLine.objects.create(
                    document=self.document,
                    vat_rate=self.vat,
                    label="line",
                    unit_price=Decimal("20.2"),
                    including_vat_fixed=True,
                    quantity=Decimal("1.42"),
                ),
                "expected_inc": Decimal("28.68"),
                "expected_exc": Decimal("23.90"),
                "expected_vat": Decimal("4.78"),
            },
            {
                "line": ComptaLine.objects.create(
                    document=self.document,
                    vat_rate=self.vat,
                    label="line",
                    unit_price=Decimal("20.2"),
                    including_vat_fixed=False,
                    quantity=Decimal("1.42"),
                ),
                "expected_inc": Decimal("34.42"),
                "expected_exc": Decimal("28.68"),
                "expected_vat": Decimal("5.74"),
            },
        ]

        vat = VatRate.objects.create(name="5.5%", rate=Decimal("1.055"))

        self.lines.extend(
            [
                {
                    "line": ComptaLine.objects.create(
                        document=self.document,
                        vat_rate=vat,
                        label="line",
                        unit_price=Decimal("20.2"),
                        including_vat_fixed=True,
                        quantity=Decimal("1.42"),
                    ),
                    "expected_inc": Decimal("28.68"),
                    "expected_exc": Decimal("27.18"),
                    "expected_vat": Decimal("1.5"),
                },
                {
                    "line": ComptaLine.objects.create(
                        document=self.document,
                        vat_rate=vat,
                        label="line",
                        unit_price=Decimal("20.2"),
                        including_vat_fixed=False,
                        quantity=Decimal("1.42"),
                    ),
                    "expected_inc": Decimal("30.26"),
                    "expected_exc": Decimal("28.68"),
                    "expected_vat": Decimal("1.58"),
                },
                {
                    "line": ComptaLine.objects.create(
                        document=self.document,
                        vat_rate=self.vat,
                        label="line",
                        unit_price=Decimal("50"),
                        including_vat_fixed=True,
                        quantity=Decimal("1"),
                    ),
                    "expected_inc": Decimal("50"),
                    "expected_exc": Decimal("41.66"),
                    "expected_vat": Decimal("8.34"),
                },
            ]
        )

        self.expected_total_inc = Decimal("216.48")
        self.expected_total_exc = Decimal("187.13")
        self.expected_total_vat = Decimal("29.35")
        self.expected_vats = {self.vat: Decimal("26.27"), vat: Decimal("3.08")}

    def test_costs(self):
        for line in self.lines:
            l = line["line"]
            self.assertEqual(l.cost_including_vat, line["expected_inc"])
            self.assertEqual(type(l.cost_including_vat), Decimal)
            self.assertEqual(l.cost_excluding_vat, line["expected_exc"])
            self.assertEqual(type(l.cost_excluding_vat), Decimal)
            self.assertEqual(l.cost_vat, line["expected_vat"])
            self.assertEqual(type(l.cost_vat), Decimal)
            self.assertEqual(l.cost_excluding_vat + l.cost_vat, l.cost_including_vat)

    def test_total_document(self):
        self.assertEqual(self.document.total_including_vat, self.expected_total_inc)
        self.assertEqual(self.document.total_excluding_vat, self.expected_total_exc)
        self.assertEqual(self.document.total_vat, self.expected_total_vat)
        self.assertEqual(
            self.document.total_vat + self.document.total_excluding_vat,
            self.document.total_including_vat,
        )
        self.assertEqual(self.document.vat_detail, self.expected_vats)
