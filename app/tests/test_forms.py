import datetime

from django.test import TestCase

from app.forms import ComptaDocumentAdminForm
from app.models import Emitter
from permissions.models import User


class TestDocumentView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.emitter = Emitter.objects.create(
            name="test emitter",
            address="hello",
            zip_code="12345",
            city="hello",
            country="hello",
            bank="hello",
            iban="FR7600000000000000000000000",
            bic="hello",
        )
        cls.username = "testuser"
        cls.password = "djangooo"
        User.objects.create_superuser(
            username=cls.username, password=cls.password, email=""
        )

    def setUp(self):
        self.client.login(username=self.username, password=self.password)

    def test_missing_prefix_triggers_message(self):
        form = ComptaDocumentAdminForm(
            {
                "client_name": "test client",
                "client_address": "test address",
                "client_zip_code": "zip code",
                "client_city": "test city",
                "client_country": "test country",
                "emitter": self.emitter.pk,
                "due_date": datetime.date(2018, 2, 2),
                "displayed_date": datetime.date(2018, 1, 2),
            }
        )
        form.is_valid()
        self.assertIn("prefix", form.errors)
