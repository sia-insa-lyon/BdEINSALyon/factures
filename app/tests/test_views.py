import datetime
from decimal import Decimal

from django.test import TestCase
from django.urls import reverse
from django.utils import formats

from app.models import (
    ComptaDocument,
    ComptaType,
    ComptaPrefix,
    Emitter,
    ComptaLine,
    VatRate,
    DueText,
)
from permissions.models import User


class TestDocumentView(TestCase):
    @classmethod
    def setUpTestData(cls):
        due_text = DueText.objects.create(
            content="{{ document.prefix.type.name }} payable le "
            "{{ document.due_date }} pour la somme de "
            "{{ document.total_including_vat|floatformat:2 }}€"
        )
        type = ComptaType.objects.create(
            name="Facture", display_bank_details=True, due_text=due_text
        )
        prefix = ComptaPrefix.objects.create(value="Fe1718/", type=type)
        emitter = Emitter.objects.create(
            name="test emitter",
            address="hello",
            zip_code="12345",
            city="hello",
            country="hello",
            bank="hello",
            iban="FR7600000000000000000000000",
            bic="hello",
        )
        cls.document = ComptaDocument.objects.create(
            client_name="test client",
            client_address="test address",
            client_zip_code="zip code",
            client_city="test city",
            client_country="test country",
            prefix=prefix,
            emitter=emitter,
            due_date=datetime.date(2018, 2, 2),
            displayed_date=datetime.date(2018, 1, 2),
        )
        cls.username = "testuser"
        cls.password = "djangooo"
        cls.vat = VatRate.objects.create(name="TVA 20%", rate=Decimal("1.2"))
        cls.line = ComptaLine.objects.create(
            document=cls.document,
            vat_rate=cls.vat,
            label="test_line",
            unit_price=Decimal("20.2"),
            including_vat_fixed=True,
            quantity=Decimal("1"),
        )
        User.objects.create_superuser(
            username=cls.username, password=cls.password, email=""
        )

    def setUp(self):
        self.client.login(username=self.username, password=self.password)
        self.resp = self.client.get(
            reverse("document-formatted", kwargs={"pk": self.document.id})
        )
        self.content = self.resp.content.decode("utf-8")

    def test_lines_displayed(self):
        self.assertIn(self.line.label, self.content)

    def test_client_data_displayed(self):
        self.assertIn(self.document.client_name, self.content)
        self.assertIn(self.document.client_address, self.content)
        self.assertIn(self.document.client_zip_code, self.content)
        self.assertIn(self.document.client_city, self.content)
        self.assertIn(self.document.client_country, self.content)

    def test_emitter_data_displayed(self):
        self.assertIn(self.document.emitter.name, self.content)
        self.assertIn(self.document.emitter.address, self.content)
        self.assertIn(self.document.emitter.zip_code, self.content)
        self.assertIn(self.document.emitter.city, self.content)
        self.assertIn(self.document.emitter.country, self.content)
        self.assertIn(self.document.emitter.bank, self.content)
        self.assertIn(self.document.emitter.iban, self.content)
        self.assertIn(self.document.emitter.bic, self.content)

    def test_document_data_displayed(self):
        self.assertIn(self.document.prefix.value, self.content)
        self.assertIn(self.document.prefix.type.name, self.content)
        self.assertIn(self.document.sequence_number_zeros, self.content)
        self.assertIn(str(self.document), self.content)
        self.assertIn(formats.date_format(self.document.due_date), self.content)
        self.assertIn(formats.date_format(self.document.displayed_date), self.content)
