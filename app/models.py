import datetime
import decimal

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.urls import reverse

from app.fields import LogoURLField
from app.validators import validate_iban


class DueText(models.Model):
    class Meta:
        verbose_name = "texte d'échéance"
        verbose_name_plural = "textes d'échéance"

    content = models.TextField(
        "contenu",
        help_text='On passe un objet de type "Document" nommé "document".'
        "Voir ce modèle pour les champs disponibles.",
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return (self.content[:75] + "...") if len(self.content) > 75 else self.content


class LegalMentions(models.Model):
    class Meta:
        verbose_name = "mentions légales"
        verbose_name_plural = "mentions légales"

    content = models.TextField(
        "contenu",
        help_text="HTML autorisé."
        "Attention, le contenu ne sera pas échappé avant d'être affiché.",
    )

    def __str__(self):
        return (self.content[:75] + "...") if len(self.content) > 75 else self.content


class BaseType(models.Model):
    class Meta:
        abstract = True
        verbose_name = "type"
        verbose_name_plural = "types"

    name = models.CharField("nom", max_length=100)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class ComptaType(BaseType):
    class Meta:
        verbose_name = "type compta"
        verbose_name_plural = "types compta"

    display_bank_details = models.BooleanField(
        "afficher les coordonnées bancaires", default=True
    )
    legal_mentions = models.ManyToManyField(
        verbose_name="mentions légales",
        to=LegalMentions,
        related_name="types",
        blank=True,
    )
    due_text = models.ForeignKey(
        verbose_name="texte d'échéance",
        to=DueText,
        on_delete=models.SET_NULL,
        related_name="types",
        null=True,
        blank=True,
    )
    payable = models.BooleanField(
        "payable", default=False, help_text="Non utilisé pour le moment."
    )
    reminder_text = models.TextField("texte de relance", blank=True)


class BasePrefix(models.Model):
    class Meta:
        abstract = True
        verbose_name = "préfixe"
        verbose_name_plural = "préfixes"

    value = models.CharField("valeur", max_length=20)
    type = models.ForeignKey(
        to=BaseType, on_delete=models.PROTECT, related_name="prefixes"
    )
    activated = models.BooleanField("actif", default=True)
    current_sequence_number = models.IntegerField(
        "numéro de séquence courant", default=0
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.value


class ComptaPrefix(BasePrefix):
    class Meta:
        verbose_name = "préfixe compta"
        verbose_name_plural = "préfixes compta"

    type = models.ForeignKey(
        to=ComptaType, on_delete=models.PROTECT, related_name="prefixes"
    )


class Emitter(models.Model):
    class Meta:
        verbose_name = "émetteur"
        verbose_name_plural = "émetteurs"

    name = models.CharField("nom", max_length=300)
    address = models.TextField("adresse")
    zip_code = models.CharField("code postal", max_length=10)
    city = models.CharField("ville", max_length=100)
    country = models.CharField("pays", max_length=100, blank=True)
    phone = models.CharField("téléphone", max_length=20, blank=True)
    fax = models.CharField("fax", max_length=20, blank=True)
    siret = models.CharField("SIRET", max_length=20, blank=True)
    vat_number = models.CharField("TVA intracommunautaire", max_length=20, blank=True)
    logo_url = LogoURLField("URL du logo", blank=True)
    bank = models.CharField("banque", max_length=100)
    iban = models.CharField("IBAN", max_length=50, validators=[validate_iban])
    bic = models.CharField("BIC/SWIFT", max_length=100)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class BaseContactInfo(models.Model):
    class Meta:
        abstract = True

    name = models.CharField("nom", max_length=200)
    email = models.EmailField("email", blank=True)
    address = models.TextField("adresse", blank=True)
    zip_code = models.CharField("code postal", max_length=10)
    city = models.CharField("ville", max_length=100)
    country = models.CharField("pays", max_length=100, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Client(BaseContactInfo):
    activated = models.BooleanField("actif", default=True)


class PaymentMethod(BaseType):
    class Meta:
        verbose_name = "méthode de paiement"
        verbose_name_plural = "méthodes de paiement"


def get_default_document_due_date():
    return datetime.date.today() + datetime.timedelta(days=30)


class BaseDocument(models.Model):
    class Meta:
        abstract = True
        verbose_name = "document"
        verbose_name_plural = "documents"

    prefix = models.ForeignKey(
        verbose_name="préfixe",
        to=BasePrefix,
        on_delete=models.PROTECT,
        related_name="documents",
    )
    sequence_number = models.IntegerField("numéro de séquence", blank=True)
    due_date = models.DateField(
        "date d'échéance", default=get_default_document_due_date
    )
    free_text = models.TextField("texte libre", blank=True, help_text="HTML autorisé")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def sequence_number_zeros(self):
        return str(self.sequence_number).zfill(3)

    def __str__(self):
        return self.prefix.value + str(self.sequence_number_zeros)


def get_default_document_emitter():
    emitter = Emitter.objects.first()
    if emitter:
        return emitter.pk
    else:
        return None


class ComptaDocument(BaseDocument):
    class Meta:
        verbose_name = "document compta"
        verbose_name_plural = "documents compta"

    prefix = models.ForeignKey(
        verbose_name="préfixe compta",
        to=ComptaPrefix,
        on_delete=models.PROTECT,
        related_name="documents",
    )
    client_name = models.CharField("nom", max_length=200, blank=True)
    client_email = models.EmailField("email", blank=True)
    client_address = models.TextField("adresse", blank=True)
    client_zip_code = models.CharField("code postal", max_length=10, blank=True)
    client_city = models.CharField("ville", max_length=100, blank=True)
    client_country = models.CharField("pays", max_length=100, blank=True)
    displayed_date = models.DateField("date affichée", default=datetime.date.today)
    payment_method = models.ForeignKey(
        to=PaymentMethod,
        on_delete=models.PROTECT,
        verbose_name="méthode de paiement",
        default=None,
        null=True,
        blank=True,
    )
    emitter = models.ForeignKey(
        verbose_name="émetteur",
        to=Emitter,
        on_delete=models.PROTECT,
        related_name="documents",
        default=get_default_document_emitter,
    )

    @property
    def total_including_vat(self):
        return sum(map(lambda x: x.cost_including_vat, self.compta_lines.all()))

    @property
    def total_excluding_vat(self):
        return sum(map(lambda x: x.cost_excluding_vat, self.compta_lines.all()))

    @property
    def total_vat(self):
        return sum(map(lambda x: x.cost_vat, self.compta_lines.all()))

    @property
    def vat_detail(self):
        rates = {}
        for rate in VatRate.objects.filter(activated=True):
            value = sum(
                map(lambda x: x.cost_vat, self.compta_lines.filter(vat_rate=rate))
            )
            if value != 0:
                rates[rate] = value
        return rates

    def get_absolute_url(self):
        return reverse("document-formatted", kwargs={"pk": str(self.id)})


@receiver(pre_save, sender=ComptaDocument)
def document_pre_save(sender, instance: ComptaDocument, **kwargs):
    if not instance.id:
        instance.prefix.current_sequence_number += 1
        instance.prefix.save()
        instance.sequence_number = instance.prefix.current_sequence_number


class VatRate(models.Model):
    class Meta:
        verbose_name = "taux de TVA"
        verbose_name_plural = "taux de TVA"

    name = models.CharField("nom", max_length=50)
    rate = models.DecimalField(
        verbose_name="taux",
        max_digits=9,
        decimal_places=3,
        default=1.0,
        help_text="Max : 999999999,999",
    )
    activated = models.BooleanField("actif", default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def percentage_display(self):
        return (self.rate - 1) * 100

    def __str__(self):
        return self.name


class BaseLine(models.Model):
    class Meta:
        abstract = True
        verbose_name = "ligne"
        verbose_name_plural = "lignes"

    label = models.CharField("libellé", max_length=500)
    unit_price = models.DecimalField(
        verbose_name="prix unitaire",
        max_digits=9,
        decimal_places=3,
        default=0.0,
        help_text="Max : 999999999,999",
    )
    including_vat_fixed = models.BooleanField(
        verbose_name="prix TTC fixé",
        default=True,
        help_text="Sinon on considère le prix HT fixé",
    )
    quantity = models.DecimalField(
        verbose_name="quantité",
        max_digits=9,
        decimal_places=3,
        default=1.0,
        help_text="Max : 999999999,999",
    )
    vat_rate = models.ForeignKey(
        verbose_name="taux de TVA",
        to=VatRate,
        on_delete=models.PROTECT,
        related_name="lines",
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def cost_including_vat(self):
        if self.including_vat_fixed:
            base_price = self.unit_price * self.quantity
        else:
            base_price = self.unit_price * self.quantity * self.vat_rate.rate
        return round(base_price, 2)

    @property
    def cost_vat(self):
        vat = self.cost_including_vat - self.cost_excluding_vat
        return (vat * 100).to_integral_exact(rounding=decimal.ROUND_CEILING) / 100

    @property
    def cost_excluding_vat(self):
        if self.including_vat_fixed:
            cost = self.cost_including_vat / self.vat_rate.rate
        else:
            cost = round(self.unit_price * self.quantity, 2)
        return (cost * 100).to_integral_exact(rounding=decimal.ROUND_FLOOR) / 100

    @property
    def unit_cost_including_vat(self):
        if self.including_vat_fixed:
            cost = self.unit_price
        else:
            cost = self.unit_price * self.vat_rate.rate
        return (cost * 100).to_integral_exact(rounding=decimal.ROUND_FLOOR) / 100

    @property
    def unit_cost_excluding_vat(self):
        if self.including_vat_fixed:
            cost = self.unit_price / self.vat_rate.rate
        else:
            cost = self.unit_price
        return (cost * 100).to_integral_exact(rounding=decimal.ROUND_FLOOR) / 100

    def __str__(self):
        return self.label


class ComptaLine(BaseLine):
    class Meta:
        verbose_name = "ligne compta"
        verbose_name_plural = "lignes compta"

    document = models.ForeignKey(
        verbose_name="document",
        to=ComptaDocument,
        on_delete=models.CASCADE,
        related_name="compta_lines",
    )
    vat_rate = models.ForeignKey(
        verbose_name="taux de TVA",
        to=VatRate,
        on_delete=models.PROTECT,
        related_name="compta_lines",
    )
