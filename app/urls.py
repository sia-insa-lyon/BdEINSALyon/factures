from django.urls import path

from .views import DocumentFormattedView, PublicHomeView

urlpatterns = [
    path("doc/<int:pk>/", DocumentFormattedView.as_view(), name="document-formatted"),
    path("", PublicHomeView.as_view(), name="public-home"),
]
