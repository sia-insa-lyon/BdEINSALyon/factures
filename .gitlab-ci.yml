include:
  - template: SAST.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml

variables:
  SAST_EXCLUDED_PATHS: account/migrations/**, app/migrations/**, demands/migrations/**, permissions/migrations/**, bash/**, uploads/**, venv/**
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      changes:
        - account/**/*
        - app/**/*
        - demands/**/*
        - factures/**/*
        - permissions/**/*
        - Pipfile
        - .gitlab-ci.yml
        - sonar-project.properties
    - if: '$CI_COMMIT_BRANCH == "master"'

code_quality:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

license_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

secret_detection:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

gemnasium-python-dependency_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

eslint-sast:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

bandit-sast:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

stages:
  - lint
  - build
  - test
  - metrics

black:
  stage: lint
  image: python:3.6
  before_script:
    - pip install black
  script:
    - black --target-version py36 --check account/ app/ demands/ factures/ permissions/ --exclude /migrations/

.build-docker: &build-docker
  stage: build
  image: docker:latest
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  services:
    - docker:dind

build-master:
  <<: *build-docker
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master

build:
  <<: *build-docker
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
  except:
    - master

test:
  stage: test
  image: python:3.6
  needs:
    - job: black
  services:
    - postgres:latest
  variables:
    DATABASE_URL: postgres://factures@postgres/factures
    POSTGRES_HOST_AUTH_METHOD: trust
    POSTGRES_DB: factures
    POSTGRES_USER: factures
  before_script:
    - pip install pipenv
    - pipenv install
    - pipenv install coverage pytest-django
    - pipenv run python manage.py migrate
  script:
    - pipenv run coverage run --source=account,app,demands,factures,permissions --omit=*/migrations/** -m pytest --junitxml=./output/junit.xml
    - pipenv run coverage report
    - pipenv run coverage xml -o output/coverage.xml
  coverage: /^TOTAL.+?(\d+\%)$/
  artifacts:
    expire_in: 1 week
    paths:
      - output
    reports:
      junit:
        - output/junit.xml

sonarcloud-check:
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  stage: metrics
  needs:
    - job: test
  dependencies:
    - test
  before_script:
    - "[ -f ./output/coverage.xml ] || (echo 'Coverage report not found' && exit 1)"
    - "[ -f ./output/junit.xml ] || (echo 'Test report not found' && exit 1)"
  script:
    - sonar-scanner
  only:
    - master
    - merge_requests
