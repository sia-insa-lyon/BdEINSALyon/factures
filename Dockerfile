FROM python:3.6-alpine
RUN apk add --no-cache postgresql-dev gcc musl-dev
RUN pip3 install pipenv
WORKDIR /app
COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv install
COPY . .
VOLUME /app/staticfiles
ENV DATABASE_URL postgres://postgresql:postgresql@db:5432/factures
EXPOSE 8000
RUN chmod +x bash/run-prod.sh
CMD /app/bash/run-prod.sh
