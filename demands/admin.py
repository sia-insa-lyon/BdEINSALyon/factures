from django.contrib import admin
from django.db.models import Q

from demands.forms import ApplicationAdminForm, ApplicationPublicForm
from demands.models import (
    ApplicationType,
    ApplicationPrefix,
    Supplier,
    PaymentMethod,
    ApplicationDocumentType,
    ApplicationLine,
    Application,
    Team,
    SpecialStatus,
    ApplicationDocument,
)
from permissions.models import User


@admin.register(ApplicationType)
class ApplicationTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(ApplicationPrefix)
class ApplicationPrefixAdmin(admin.ModelAdmin):
    pass


@admin.register(Supplier)
class SupplierAdmin(admin.ModelAdmin):
    pass


@admin.register(PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    pass


class ApplicationLineInline(admin.TabularInline):
    model = ApplicationLine

    def get_extra(self, request, obj=None, **kwargs):
        if obj and (obj.validation_resp or obj.validation_trez or obj.validation_prez):
            return 0
        return super().get_extra(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj and (
            (
                obj.validation_resp
                and request.user != obj.team.resp
                and request.user
                not in User.objects.filter(
                    Q(special_statuses__type=SpecialStatus.PRESIDENT)
                    | Q(special_statuses__type=SpecialStatus.TRESORIER)
                )
            )
            or (
                (obj.validation_trez or obj.validation_prez)
                and request.user
                not in User.objects.filter(
                    Q(special_statuses__type=SpecialStatus.PRESIDENT)
                    | Q(special_statuses__type=SpecialStatus.TRESORIER)
                )
            )
        ):
            return [
                "label",
                "unit_price",
                "including_vat_fixed",
                "quantity",
                "vat_rate",
                "type",
            ]
        return super().get_readonly_fields(request, obj)

    def has_delete_permission(self, request, obj=None):
        if obj and (obj.validation_resp or obj.validation_trez or obj.validation_prez):
            return False
        return super().get_extra(request, obj)


class ApplicationDocumentInline(admin.StackedInline):
    model = ApplicationDocument
    extra = 1

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj is None:
            return False
        return obj.enquirer == request.user


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    inlines = [
        ApplicationLineInline,
        ApplicationDocumentInline,
    ]
    list_display = [
        "__str__",
        "supplier",
        "enquirer",
        "team",
        "due_date",
        "total_including_vat",
        "validation_resp",
        "validation_prez",
        "validation_trez",
        "created_at",
        "updated_at",
    ]
    list_filter = [
        "prefix",
        "team",
        "validation_resp",
        "validation_prez",
        "validation_trez",
    ]
    search_fields = [
        "sequence_number",
    ]
    date_hierarchy = "due_date"

    def get_fieldsets(self, request, obj=None):
        if request.user.is_superuser:
            fieldsets = [
                (
                    "Informations générales",
                    {
                        "fields": (
                            "prefix",
                            "due_date",
                            ("team", "enquirer"),
                            "created_at",
                            "updated_at",
                        )
                    },
                ),
            ]
        else:
            fieldsets = [
                (
                    "Informations générales",
                    {
                        "fields": (
                            "prefix",
                            "due_date",
                            "team",
                            "created_at",
                            "updated_at",
                        )
                    },
                )
            ]
        fieldsets.extend(
            [
                ("Fournisseur", {"fields": ("supplier",)}),
                ("Pour quoi ?", {"fields": ("motive", "details")}),
                ("Paiement", {"fields": ("payment_method",)}),
                (
                    "Validation",
                    {
                        "fields": (
                            "validation_resp",
                            "validation_prez",
                            "validation_trez",
                        )
                    },
                ),
            ]
        )
        self.fieldsets = fieldsets

        return super().get_fieldsets(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            self.form = ApplicationAdminForm
        else:
            self.form = ApplicationPublicForm
        form = super().get_form(request, obj, **kwargs)
        form.request = request
        return form

    def save_model(self, request, obj, form, change):
        instance = form.save(commit=False)
        if not change or not instance.enquirer:
            user = request.user
            instance.enquirer = user

        instance.save()
        form.save_m2m()
        return instance

    def get_readonly_fields(self, request, obj=None):
        readonly = ["created_at", "updated_at"]
        if request.user.is_superuser or request.user in User.objects.filter(
            Q(special_statuses__type=SpecialStatus.PRESIDENT)
            | Q(special_statuses__type=SpecialStatus.TRESORIER)
        ):
            return readonly
        if obj and obj.team and obj.team.resp == request.user:
            readonly.extend(["validation_prez", "validation_trez"])
        else:
            readonly.extend(["validation_prez", "validation_trez", "validation_resp"])
        return readonly

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser or request.user in User.objects.filter(
            Q(special_statuses__type=SpecialStatus.PRESIDENT)
            | Q(special_statuses__type=SpecialStatus.TRESORIER)
        ):
            return qs
        return qs.filter(
            Q(team__members=request.user)
            | Q(enquirer=request.user)
            | Q(team__resp=request.user)
        ).distinct()


@admin.register(ApplicationLine)
class ApplicationLineAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(application__enquirer=request.user)


@admin.register(SpecialStatus)
class SpecialStatusAdmin(admin.ModelAdmin):
    pass


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    filter_horizontal = [
        "members",
    ]


@admin.register(ApplicationDocumentType)
class ApplicationDocumentTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(ApplicationDocument)
class ApplicationDocumentAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)

        if request.user.is_superuser or request.user in User.objects.filter(
            Q(special_statuses__type=SpecialStatus.PRESIDENT)
            | Q(special_statuses__type=SpecialStatus.TRESORIER)
        ):
            return qs

        allowed_applications = Application.objects.filter(
            Q(team__members=request.user)
            | Q(enquirer=request.user)
            | Q(team__resp=request.user)
        )
        return qs.filter(application__in=allowed_applications)
