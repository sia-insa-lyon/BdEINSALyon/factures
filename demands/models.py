import datetime
import uuid

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver

from app.models import (
    BasePrefix,
    BaseType,
    BaseContactInfo,
    BaseDocument,
    document_pre_save,
    BaseLine,
    VatRate,
)
from permissions.models import User, AzureGroup


class ApplicationType(BaseType):
    class Meta:
        verbose_name = "type demande"
        verbose_name_plural = "type demande"


class ApplicationPrefix(BasePrefix):
    class Meta:
        verbose_name = "préfixe demande"
        verbose_name_plural = "préfixes demande"

    type = models.ForeignKey(
        to=ApplicationType, on_delete=models.PROTECT, related_name="prefixes"
    )


class Supplier(BaseContactInfo):
    class Meta:
        verbose_name = "fournisseur"
        verbose_name_plural = "fournisseurs"

    activated = models.BooleanField("actif", default=True)


class PaymentMethod(BaseType):
    class Meta:
        verbose_name = "méthode de paiement"
        verbose_name_plural = "méthodes de paiement"


class SpecialStatus(models.Model):
    class Meta:
        verbose_name = "statut spécial"
        verbose_name_plural = "statuts spéciaux"

    PRESIDENT = "PR"
    TRESORIER = "TR"
    VIEW_ONLY = "VO"

    _TYPE_CHOICES = (
        (PRESIDENT, "Président"),
        (TRESORIER, "Trésorier"),
        (VIEW_ONLY, "Lecture seule (non implémenté)"),
    )
    type = models.CharField("type", max_length=3, choices=_TYPE_CHOICES)
    user = models.ForeignKey(
        verbose_name="utilisateur",
        to=User,
        on_delete=models.CASCADE,
        related_name="special_statuses",
    )

    def __str__(self):
        return str(self.user) + " -> " + str(self.type)


class Team(models.Model):
    class Meta:
        verbose_name = "équipe"
        verbose_name_plural = "équipes"

    name = models.CharField("nom", max_length=200)
    resp = models.ForeignKey(
        verbose_name="responsable",
        to=User,
        on_delete=models.PROTECT,
        related_name="teams_resp",
    )
    members = models.ManyToManyField(
        verbose_name="membres",
        to=User,
        related_name="teams",
        help_text="Si un groupe Azure est spécifié, les utilisateurs seront automatiquement mis à jour. "
        "Toute modification manuelle sera donc écrasée.",
    )
    azure_group = models.ForeignKey(
        verbose_name="groupe azure",
        to=AzureGroup,
        on_delete=models.SET_NULL,
        related_name="teams",
        blank=True,
        null=True,
    )

    def __str__(self):
        return "{} (resp : {})".format(
            self.name, self.resp.get_full_name() or self.resp.username
        )


def get_default_application_due_date():
    return datetime.date.today() + datetime.timedelta(days=7)


class Application(BaseDocument):
    class Meta:
        verbose_name = "demande"
        verbose_name_plural = "demandes"

    prefix = models.ForeignKey(
        verbose_name="préfixe",
        to=ApplicationPrefix,
        on_delete=models.PROTECT,
        related_name="applications",
    )
    enquirer = models.ForeignKey(
        verbose_name="demandeur",
        to=User,
        on_delete=models.PROTECT,
        related_name="applications",
    )
    team = models.ForeignKey(
        verbose_name="équipe",
        to=Team,
        on_delete=models.PROTECT,
        related_name="applications",
    )
    supplier = models.ForeignKey(
        verbose_name="fournisseur",
        to=Supplier,
        on_delete=models.PROTECT,
        related_name="applications",
    )
    payment_method = models.ForeignKey(
        verbose_name="méthode de paiement",
        to=PaymentMethod,
        on_delete=models.PROTECT,
        related_name="applications",
    )
    due_date = models.DateField(
        "date d'échéance", default=get_default_application_due_date
    )
    motive = models.CharField("motif", max_length=200)
    details = models.TextField("details", blank=True)
    validation_prez = models.BooleanField("validation président", default=False)
    validation_prez_updated_at = models.DateTimeField(
        "validation président modifié à", null=True, blank=True, editable=False
    )
    validation_prez_updated_by = models.ForeignKey(
        verbose_name="validation président modifié par",
        to=User,
        on_delete=models.SET_NULL,
        related_name="applications_validation_prez",
        null=True,
        editable=False,
    )
    validation_trez = models.BooleanField("validation trésorier", default=False)
    validation_trez_updated_at = models.DateTimeField(
        "validation trésorier modifié à", null=True, blank=True, editable=False
    )
    validation_trez_updated_by = models.ForeignKey(
        verbose_name="validation trésorier modifié par",
        to=User,
        on_delete=models.SET_NULL,
        related_name="applications_validation_trez",
        null=True,
        editable=False,
    )
    validation_resp = models.BooleanField("validation responsable", default=False)
    validation_resp_updated_at = models.DateTimeField(
        "validation responsable modifié à", null=True, blank=True, editable=False
    )
    validation_resp_updated_by = models.ForeignKey(
        verbose_name="validation responsable modifié par",
        to=User,
        on_delete=models.SET_NULL,
        related_name="applications_validation_resp",
        null=True,
        editable=False,
    )

    @property
    def total_including_vat(self):
        return sum(map(lambda x: x.cost_including_vat, self.application_lines.all()))

    @property
    def total_excluding_vat(self):
        return sum(map(lambda x: x.cost_excluding_vat, self.application_lines.all()))

    @property
    def total_vat(self):
        return sum(map(lambda x: x.cost_vat, self.application_lines.all()))

    @property
    def vat_detail(self):
        rates = {}
        for rate in VatRate.objects.filter(activated=True):
            value = sum(
                map(lambda x: x.cost_vat, self.application_lines.filter(vat_rate=rate))
            )
            if value != 0:
                rates[rate] = value
        return rates


@receiver(pre_save, sender=Application)
def demand_pre_save(sender, instance, **kwargs):
    document_pre_save(sender, instance, **kwargs)


class ApplicationLine(BaseLine):
    class Meta:
        verbose_name = "ligne de demande"
        verbose_name_plural = "lignes de demande"

    TYPE_FUEL = "FU"
    TYPE_DISTANCE = "KM"
    TYPE_MERCHANDISE = "ME"
    _TYPE_CHOICES = (
        (TYPE_MERCHANDISE, "Articles"),
        (TYPE_FUEL, "Carburant"),
        (TYPE_DISTANCE, "Frais kilométriques"),
    )
    type = models.CharField(
        "type", max_length=3, choices=_TYPE_CHOICES, default=TYPE_MERCHANDISE
    )
    application = models.ForeignKey(
        verbose_name="demande",
        to=Application,
        on_delete=models.CASCADE,
        related_name="application_lines",
    )
    vat_rate = models.ForeignKey(
        verbose_name="taux de TVA",
        to=VatRate,
        on_delete=models.PROTECT,
        related_name="application_lines",
    )


class ApplicationDocumentType(BaseType):
    class Meta:
        verbose_name = "type de document"
        verbose_name_plural = "types de document"


def user_directory_path(instance, filename):
    return ".".join([str(uuid.uuid1()), filename.split(".")[-1]])


class ApplicationDocument(models.Model):
    class Meta:
        verbose_name = "document"
        verbose_name_plural = "documents"

    file = models.FileField(verbose_name="fichier", upload_to=user_directory_path)
    type = models.ForeignKey(
        verbose_name="type de document",
        to=ApplicationDocumentType,
        on_delete=models.PROTECT,
        related_name="application_documents",
    )
    application = models.ForeignKey(
        verbose_name="demande",
        to=Application,
        on_delete=models.CASCADE,
        related_name="application_documents",
    )
    details = models.TextField("détails", blank=True)
