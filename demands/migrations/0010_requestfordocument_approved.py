# Generated by Django 2.0.1 on 2018-02-01 08:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demands', '0009_auto_20180201_0930'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestfordocument',
            name='approved',
            field=models.BooleanField(default=False, verbose_name='approuvé'),
        ),
    ]
