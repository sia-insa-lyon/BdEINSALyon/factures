# Generated by Django 2.0.1 on 2018-02-01 20:29

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demands', '0014_auto_20180201_2127'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestfordocument',
            name='application',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='requests_for_document', to='demands.Application', verbose_name='demande'),
        ),
        migrations.AlterField(
            model_name='requestfordocumenttype',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='requests_for_document_types', to='demands.ApplicationDocumentType', verbose_name='type'),
        ),
    ]
