# Generated by Django 2.0.5 on 2018-05-19 09:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demands', '0027_auto_20180519_1037'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='applicationtype',
            name='default_requests_for_document',
        ),
        migrations.AddField(
            model_name='applicationtype',
            name='default_requests_for_document_types',
            field=models.ManyToManyField(related_name='application_types', to='demands.RequestForDocumentType'),
        ),
    ]
