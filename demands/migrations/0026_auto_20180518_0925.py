# Generated by Django 2.0.5 on 2018-05-18 07:25

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demands', '0025_team_azure_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='members',
            field=models.ManyToManyField(help_text='Si un groupe Azure est spécifié, les utilisateurs seront automatiquement mis à jour. Toute modification manuelle sera donc écrasée.', related_name='teams', to=settings.AUTH_USER_MODEL),
        ),
    ]
