# Generated by Django 2.0.1 on 2018-02-01 20:30

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demands', '0015_auto_20180201_2129'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='team',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='applications', to='demands.Team', verbose_name='équipe'),
            preserve_default=False,
        ),
    ]
