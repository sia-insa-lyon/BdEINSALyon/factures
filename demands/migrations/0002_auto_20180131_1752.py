# Generated by Django 2.0.1 on 2018-01-31 16:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('demands', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='applicationdocument',
            options={'verbose_name': 'document', 'verbose_name_plural': 'documents'},
        ),
        migrations.AlterModelOptions(
            name='applicationline',
            options={'verbose_name': 'ligne de demande', 'verbose_name_plural': 'lignes de demande'},
        ),
    ]
