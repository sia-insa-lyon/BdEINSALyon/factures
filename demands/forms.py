from django import forms
from django.db.models import Q

from demands.models import Application, ApplicationPrefix, Supplier, SpecialStatus
from permissions.models import User


class ApplicationPublicForm(forms.ModelForm):
    class Meta:
        model = Application
        exclude = ["enquirer"]

    prefix = forms.ModelChoiceField(
        ApplicationPrefix.objects.filter(activated=True).distinct().order_by("value"),
        label="Préfixe",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["supplier"].queryset = Supplier.objects.filter(
            activated=True
        ).order_by("name")
        if self.instance.pk:
            self.fields["prefix"].queryset = (
                ApplicationPrefix.objects.filter(
                    Q(activated=True) | Q(applications=self.instance)
                )
                .distinct()
                .order_by("value")
            )
            self.fields["prefix"].initial = self.instance.prefix

    def clean(self):
        data = self.cleaned_data
        if self.request.user.is_superuser:
            return data

        if not self.instance:
            return data

        # Prevent unwanted modification from others than prez/trez if a prez/trez validation has been done
        if (
            self.instance.validation_trez
            or self.instance.validation_prez
            and self.request.user
            not in User.objects.filter(
                Q(special_statuses__type=SpecialStatus.PRESIDENT)
                | Q(special_statuses__type=SpecialStatus.TRESORIER)
            )
        ):
            if self.request.user == self.instance.team.resp and self.changed_data == [
                "validation_resp"
            ]:
                return data
            else:
                for field in set(self.changed_data) - {"validation_resp"}:
                    self.add_error(
                        field,
                        forms.ValidationError(
                            "Impossible de modifier une demande validée par le Président ou le Trésorier."
                        ),
                    )
            raise forms.ValidationError(
                "Impossible de modifier une demande validée par le Président ou le Trésorier."
            )

        # Prevent unwanted modification from others than resp/prez/trez if a resp validation has been done
        if (
            self.instance.validation_resp
            and self.request.user
            not in User.objects.filter(
                Q(special_statuses__type=SpecialStatus.PRESIDENT)
                | Q(special_statuses__type=SpecialStatus.TRESORIER)
            )
            and self.request.user != self.instance.team.resp
        ):
            for field in self.changed_data:
                self.add_error(
                    field,
                    forms.ValidationError(
                        "Impossible de modifier une demande validée par le Président, le Trésorier ou le Resp."
                    ),
                )
            raise forms.ValidationError(
                "Impossible de modifier une demande validée par le Président, le Trésorier ou le Resp."
            )

        return data


class ApplicationAdminForm(ApplicationPublicForm):
    class Meta:
        model = Application
        fields = "__all__"
