from django.apps import AppConfig


class DemandsConfig(AppConfig):
    name = "demands"
    verbose_name = "Demandes"
